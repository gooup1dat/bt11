

document.addEventListener("DOMContentLoaded", function() {
    function rangeValue(value) {
        document.getElementById("range-value").innerText = value+"$";
    }

    function validateExperience() {
        var experience = document.getElementById("experience");
        if (experience.value === "") {
            return false;
        } else {
            return true;
        }
    }

    document.getElementById("form").addEventListener("submit", function() {
        event.preventDefault();
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var favoriteColor = document.getElementById("favorite-color").value;
        var dob = document.getElementById("birthday").value;
        var gender = document.querySelector('input[name="gender"]:checked').value;
        var experience = document.getElementById("experience");    
        
        var interest = document.querySelectorAll('input[name="interest"]:checked');
        var interestValues = [];
        for (var i = 0; i < interest.length; i++) {
            interestValues.push(interest[i].value);
        }
    
        var budget = document.getElementById("budget").value;
        
        var data = {
            name,
            email,
            favoriteColor,
            dob,
            gender,
            interest: interestValues,
            budget
        }

        if (validateExperience()) {
            data.experience = experience.options[experience.selectedIndex].value;
        }
        
        console.log(data);
    });

    document.getElementById("budget").addEventListener("input", function() {
        rangeValue(this.value);
    });
});

